local options = {
  extensions_list = { "themes", "terms", "ui-select", "undo" },
}

return options
