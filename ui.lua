local function padAsciiArt(asciiArt, m, n)
	local artLines = asciiArt
	local numRows = #artLines
	local numCols = #artLines[1]

	-- Calculate padding
	local rowPad = math.floor((m - numRows) / 2)
	local colPad = math.floor((n - numCols) / 2)

	-- Initialize the padded grid
	local paddedGrid = {}
	for i = 1, m do
		paddedGrid[i] = {}
		for j = 1, n do
			paddedGrid[i][j] = " "
		end
	end

	-- Place ASCII art in the center
	for i = 1, numRows do
		for j = 1, numCols do
			paddedGrid[i + rowPad][j + colPad] = artLines[i]:sub(j, j)
		end
	end

	-- Convert the grid back to a table of strings
	local paddedAsciiArt = {}
	for i = 1, m do
		table.insert(paddedAsciiArt, table.concat(paddedGrid[i]))
	end

	return paddedAsciiArt
end

local function getRandomEntry(tableList)
	math.randomseed(os.time()) -- Seed the random number generator with the current time
	local randomIndex = math.random(1, #tableList)
	return tableList[randomIndex]
end

local function choosePenguin()
	local penguins = {
		{
			"(o_",
			"//\\",
			"V_/_", -- Standard Penguin
		},
		{
			"(*_",
			"//\\",
			"V_/_", -- Pissed Penguin  (Drunk)
		},
		{
			"(O_",
			"//\\",
			"V_/_", -- Shocked Penguin
		},
		{
			"(o<",
			"//\\",
			"V_/_", -- Noisy Penguin
		},
		{
			"(o<)<",
			"//\\",
			"V_/_", -- Penguin Eating Fish
		},
		{
			"_o)",
			"//\\",
			"V_/_", -- Paranoid Penguin
		},
		{
			"          (o_",
			"(o_  (o_  //\\",
			"(/)_ (/)_ V_/_", -- The Penguin Family
		},
		{
			"(o|",
			"//\\",
			"V_/_", -- Penguin after hitting a wall
		},
	}
	local chosen_penguin = getRandomEntry(penguins)
	return padAsciiArt(chosen_penguin, 9, 37)
end

return {
	------------------------------- nvchad_ui modules -----------------------------
	statusline = {
		theme = "vscode_colored", -- default/vscode/vscode_colored/minimal
	},

	-- lazyload it when there are 1+ buffers
	tabufline = {
		show_numbers = false,
		enabled = true,
		lazyload = true,
		overriden_modules = nil,
	},

	-- nvdash (dashboard)
	nvdash = {
		load_on_startup = true,

		header = choosePenguin(),
		-- header = {
		-- 	"           ▄ ▄                   ",
		-- 	"       ▄   ▄▄▄     ▄ ▄▄▄ ▄ ▄     ",
		-- 	"       █ ▄ █▄█ ▄▄▄ █ █▄█ █ █     ",
		-- 	"    ▄▄ █▄█▄▄▄█ █▄█▄█▄▄█▄▄█ █     ",
		-- 	"  ▄ █▄▄█ ▄ ▄▄ ▄█ ▄▄▄▄▄▄▄▄▄▄▄▄▄▄  ",
		-- 	"  █▄▄▄▄ ▄▄▄ █ ▄ ▄▄▄ ▄ ▄▄▄ ▄ ▄ █ ▄",
		-- 	"▄ █ █▄█ █▄█ █ █ █▄█ █ █▄█ ▄▄▄ █ █",
		-- 	"█▄█ ▄ █▄▄█▄▄█ █ ▄▄█ █ ▄ █ █▄█▄█ █",
		-- 	"    █▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄█ █▄█▄▄▄█    ",
		-- },
	},
}
