local plugins = {
	{ "tpope/vim-fugitive", cmd = "G" },
	{ "tpope/vim-surround", lazy = false },
	require("custom.config.conform"),
	require("custom.config.zk"),
	{ "folke/tokyonight.nvim", lazy = false },
	{
		"neovim/nvim-lspconfig",
		config = function()
			require("plugins.configs.lspconfig")
			require("custom.config.lspconfig")
		end,
	},
	{ "nvim-telescope/telescope-ui-select.nvim" },
	{ "debugloop/telescope-undo.nvim" },
	{
		"nvim-telescope/telescope.nvim",
		opts = function()
			return require("custom.config.telescope")
		end,
	},
	{
		"williamboman/mason.nvim",
		dependencies = {
			"nvim-telescope/telescope.nvim",
		},
	},
}

return plugins
